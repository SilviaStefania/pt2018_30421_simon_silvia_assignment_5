package PT2018Tema5;

import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.StrictMath.abs;

public class MonitoredData {

    private String startTime;
    private String endTime;
    private String activity;




    public MonitoredData(String startTime, String endTime, String activity){
       this.activity = activity;
       this.endTime =endTime;
       this.startTime = startTime;
    }


    public String adunare (String end1, String start2){

        int ora1 ;
        int min1 ;
        int sec1;

        if(end1.length() == 8){
              ora1 = Integer.parseInt(end1.substring(0 , 2));
              min1 = Integer.parseInt(end1.substring(3 , 5));
              sec1= Integer.parseInt(end1.substring(6 , 8));
        }
        else {
             ora1 = Integer.parseInt(end1.substring(0 , 3));
             min1 = Integer.parseInt(end1.substring(4 , 6));
             sec1= Integer.parseInt(end1.substring(7 , 9));
        }


        int ora2 =Integer.parseInt(start2.substring(0 , 2));
        int min2 =Integer.parseInt(start2.substring(3 , 5));
        int sec2=Integer.parseInt(start2.substring(6 , 8));


        int remainder = 0;
        int sec;
        int min;
        int ora;

        if((sec1+sec2)<60){
            sec= (sec1+sec2);
        }
        else {
            sec=  (sec1+sec2)%60 ;remainder=1;
        }

        if((min1+min2+remainder)<60){
            min= (min1+min2+remainder);
            remainder=0;
        }
        else {
            min= (min1+min2+remainder)%60 ;remainder=1;
        }

            ora= (ora1+ora2+remainder);


        String scadereRez="";
        if(ora<10)
            scadereRez+="0"+ora+":";
        else
            scadereRez+=ora+":";
        if(min<10)
            scadereRez+="0"+min+":";
        else
            scadereRez+=min+":";
        if(sec<10)
            scadereRez+="0"+sec;
        else
            scadereRez+=sec;

        return scadereRez;


    }


    public String scadere (String end1, String start2){
        int ora1 = Integer.parseInt(end1.substring(11 , 13));
        int ora2 =Integer.parseInt(start2.substring(11 , 13));
        int min1 = Integer.parseInt(end1.substring(14 , 16));
        int min2 =Integer.parseInt(start2.substring(14 , 16));
        int sec1= Integer.parseInt(end1.substring(17 , 19));
        int sec2=Integer.parseInt(start2.substring(17 , 19));

        int remainder = 0;
        int sec;
        int min;
        int ora;

        if((sec1-sec2)>0){
            sec= (sec1-sec2);
        }
        else {
            sec=  abs(sec1-sec2) ;remainder=1;
        }

        if((min1-min2-remainder)>=0){
            min= (min1-min2-remainder);
            remainder=0;
        }
        else {
            min= abs(min1-min2-remainder) ;remainder=1;
        }


        if((ora1-ora2-remainder)>0){
            ora= (ora1-ora2-remainder);
        }
        else {
            ora= abs(ora1-ora2-remainder);
        }


        String scadereRez="";
        if(ora<10)
             scadereRez+="0"+ora+":";
        else
            scadereRez+=ora+":";
        if(min<10)
            scadereRez+="0"+min+":";
        else
            scadereRez+=min+":";
        if(sec<10)
            scadereRez+="0"+sec;
        else
            scadereRez+=sec;

        return scadereRez;
    }



    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}











