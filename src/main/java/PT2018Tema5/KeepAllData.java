package PT2018Tema5;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;


public class KeepAllData {

    private ArrayList<MonitoredData> monitoredData = new ArrayList<>();
    private ArrayList<String> startTimeCatezile = new ArrayList<>();


    Map<String, Integer> activityCounter = new HashMap<>();
    Map<String, String> totalTimePerActivity = new HashMap<>();

    Map<Integer,  Map<String, Integer>> eachDayAcApp = new HashMap<>();


    public KeepAllData() throws FileNotFoundException {
        String fileName = "test.txt";
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(linie -> monitoredData.add( new MonitoredData( linie.substring(0, 19),
                                                                        linie.substring(21, 40),
                                                                        linie.substring(42))     )
                                                    );

        } catch (IOException e) {
            e.printStackTrace();
        }



        //numara cate zile diferite apar
        monitoredData.stream().forEach(linie ->{startTimeCatezile.add(linie.getStartTime().substring(8, 10));
                                       //  afisare pe linie cat dureazaacea activitate
                                        System.out.println(linie.scadere(linie.getEndTime(), linie.getStartTime() +"\n"  )) ;

        } );




        HashMap<String,Integer> lessThan5 = new HashMap<>();
        String[] deVerif =  {"Leaving\t", "Breakfast\t", "Grooming\t", "Showering\t", "Lunch\t", "Spare_Time/TV\t", "Toileting\t", "Sleeping\t", "Snack\t"};
        List<String> rezuLess = new ArrayList<>();

        Stream<String> cateZileDif = startTimeCatezile.stream().distinct();
        Stream<String>  fiecareZi = startTimeCatezile.stream().distinct();
        System.out.println("Number of different days: "+cateZileDif.count());



        monitoredData.stream().forEach((k)-> {
            //ce activitati apar si de cate ori
            if(activityCounter.get(k.getActivity()) != null){
                activityCounter.put(k.getActivity(),activityCounter.get(k.getActivity())+1);
            }else
                activityCounter.put(k.getActivity(), 1);

            //durata fiecarui timp in toate zilele
            if(totalTimePerActivity.get(k.getActivity())!=null){
                totalTimePerActivity.put(k.getActivity(), k.adunare(totalTimePerActivity.get(k.getActivity()), k.scadere(k.getEndTime(), k.getStartTime()))) ;

            }
            else
                totalTimePerActivity.put(k.getActivity(), k.scadere(k.getEndTime(), k.getStartTime())) ;

            //90 la suta
                if(k.scadere(k.getEndTime(), k.getStartTime()).substring(1, 2).equals("0") && Integer.parseInt(k.scadere(k.getEndTime(), k.getStartTime()).substring(3, 5) )  <=5){
                    if(lessThan5.get(k.getActivity())!= null) {
                        lessThan5.put(k.getActivity(), lessThan5.get(k.getActivity())+1);
                    }
                    else
                        lessThan5.put(k.getActivity(), 1);

                }

        } );

        lessThan5.entrySet().stream().forEach( k -> {
            for(int i = 0; i<9; i++){
                if(deVerif[i].equals(k.getKey())){
                    if(k.getValue() <= activityCounter.get(k.getKey())  * 0.9){
                        rezuLess.add(k.getKey());
                    }
                }
            }

        });

        PrintWriter writer5 = new PrintWriter("90laSuta.txt");
        writer5.println(" 90% less then 5 " + rezuLess.size());
        rezuLess.stream().forEach(k-> writer5.println(k));
        writer5.close();


        //de cate ori apare fiecxare task pe zile
        fiecareZi.forEach((k)-> {
            Map<String, Integer>  miniMap = new HashMap<>();
            monitoredData.stream().forEach(u -> {
                if (u.getStartTime().substring(8, 10).equals(k)) {
                    if (miniMap.get(u.getActivity()) != null) {
                        miniMap.put(u.getActivity(), miniMap.get(u.getActivity()) + 1);
                    } else
                        miniMap.put(u.getActivity(), 1);
                }
            });
            eachDayAcApp.put(Integer.parseInt(k), miniMap);

        });





        PrintWriter writer3 = new PrintWriter("Each day count.txt");
        writer3.println("Total apparitions of each activity BY DAY ");
        eachDayAcApp.entrySet().stream().forEach(k-> writer3.println(k.getKey()+" "+k.getValue()));
        writer3.close();


        PrintWriter writer = new PrintWriter("NR of apparitions.txt");
        writer.println("Total apparitions of each activity ");
        activityCounter.entrySet().stream().forEach(k -> writer.println( k.getKey()+"    " + k.getValue()));
        writer.close();

        PrintWriter writer2 = new PrintWriter("Total duration.txt");
        writer2.println("Total time for each activity ");
        totalTimePerActivity.entrySet().stream().forEach(k->{
                    if(!(k.getValue().substring(0, 1).equals("0"))){
                        writer2.println(k.getKey()+"    " + k.getValue());
                    }
                }
               );
        writer2.close();





    }





}
